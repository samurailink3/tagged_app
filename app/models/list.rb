class List < ActiveRecord::Base
  belongs_to :user
  has_many :items, :dependent => :destroy
  accepts_nested_attributes_for :items, :allow_destroy => true


  def is_owned_by(current_user)
    if current_user.id === self.user_id
      return true
    else
      return false
    end
  end

end
