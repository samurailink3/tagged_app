Tagged App
==========

Tagged is a tagged list manager on the web, powered by [Ruby on Rails](http://rubyonrails.org/).

## What does it do?

Plain and simple: Make a list of stuff, filter it by tags, the app chooses one at random, that's your choice. It helps you make semi-informed decisions. 

## Why?

I'm in the middle of a big Rails kick, so I decided to take on a personal challenge: Actually finish a project (even a small one). I had a problem: My girlfriend and I can't ever decide where to eat. My solution: Build a filterable list that will make the decision for us.

## Documentation

Most documentation for this project will be done using the [wiki on the GitHub Project Page](https://github.com/samurailink3/tagged_app/wiki). This documentation will be made available in the `/doc` directory with each new version.

In-app documentation will be handled with [TomDoc-1.0.0-rc1](http://tomdoc.org/).

## Versioning

This project is using [Semantic Versioning 2.0](http://semver.org/spec/v2.0.0.html).

The most current release is **1.2.0**