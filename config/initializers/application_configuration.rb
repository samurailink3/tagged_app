module ApplicationConfiguration
  class Application < Rails::Application
    config.before_configuration do
      conf_file = Rails.root.join("config", 'application_configuration.yml').to_s
      if File.exists?(conf_file)
        YAML.load_file(conf_file)[Rails.env].each do |key, value|
          ENV[key.to_s] = value
        end
      end
    end
  end
end